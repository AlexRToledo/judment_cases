const express = require('express'),
    Express = require('./config/lib/Express'),
    Router = require('./config/routes/Router'),
    Logger  = require('./config/lib/Logger'),
    Db = require('./config/db/Db'),
    CronSchedule = require('./app/main/helpers/cron/Cron'),
    app = express();

class App {
    constructor() {
        Db.Initialize().then(_ =>{
            new Express(app);
            new Router(app);
            new CronSchedule();

            Logger.Initialize(app);

            app.use(function (req, res, next) {
                res.render('error/404');
            });

            if (app.get('env') === 'development') {
                app.use(function (err, req, res, next) {
                    res.status(err.status || 500);
                    console.log(err.message);
                    res.render('error/500', {
                        message: err.message,
                        error: err,
                        title: 'Page 500'
                    });
                });
            }
        }).catch(err => {
            console.error(err);
        });
        return app;
    }
}

module.exports = App;





