global.config = require('./config.json');

const mongoose = require('mongoose');
const Models = require('./config/db/Models');
const UserRepository = require('./app/main/repository/UserRepository');
const UserModel = require('./app/main/models/UserModel');

mongoose.connect(config.mongoUrl, { useNewUrlParser: true, useCreateIndex: true });
global.db = new Models(mongoose).Load();

const userRepository = new UserRepository();

const seed = async () => {
    await userRepository.Create({
        name: 'Admin',
        email: 'admin@localhost.com',
        password: 'admin123',
        role: UserModel.Roles.Administrator,
        isActive: true
    });
};

seed().then(() => {
    console.log('seeded');
    process.exit(0);
}).catch((e) => {
    console.log(e);
    process.exit(1);
});
