//Api
const AuthController = require('../../app/main/controllers/Api/AuthController');
const CaseController = require('../../app/main/controllers/Api/CaseController');
const NotificationController = require('../../app/main/controllers/Api/NotificationController');
const OfficeController = require('../../app/main/controllers/Api/OfficeController');
const UserController = require('../../app/main/controllers/Api/UserController');
const FolderController = require('../../app/main/controllers/Api/FolderController');
const NotesController = require('../../app/main/controllers/Api/NotesController');

//Web
const MainController = require('../../app/main/controllers/Web/MainController');

class Router {
    constructor(app) {
        const api = '/api/v1';

        app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', '*');
            res.header('Access-Control-Allow-Headers', '*');
            next();
        });


        //Api
        new AuthController(app).Router(`${api}/auth`);
        new CaseController(app).Router(`${api}/cases`);
        new NotificationController(app).Router(`${api}/notify`);
        new OfficeController(app).Router(`${api}/offices`);
        new UserController(app).Router(`${api}/users`);
        new FolderController(app).Router(`${api}/folders`);
        new NotesController(app).Router(`${api}/notes`);

        //Web
        new MainController(app).Router(`/`);
    }
}

module.exports = Router;
