const UserModel = require('../../app/main/models/UserModel');
const OfficeModel = require('../../app/main/models/OfficeModel');
const CaseModel = require('../../app/main/models/CaseModel');
const NotificationModel = require('../../app/main/models/NotificationModel');
const FolderCasesModel = require('../../app/main/models/FolderCasesModel');
const FileCasesModel = require('../../app/main/models/FileCasesModel');
const NotesModel = require('../../app/main/models/NotesModel');
const CommentModel = require('../../app/main/models/CommentModel');
const TicketsModel = require('../../app/main/models/TicketsModel');

class Models {
    constructor(mongoose) {
        this.mongoose = mongoose;
    }

    Load() {
        return {
            mongoose: this.mongoose,
            Users: new UserModel(this.mongoose),
            Offices: new OfficeModel(this.mongoose),
            Cases: new CaseModel(this.mongoose),
            Notifications: new NotificationModel(this.mongoose),
            FolderCases: new FolderCasesModel(this.mongoose),
            FilesCases: new FileCasesModel(this.mongoose),
            Notes: new NotesModel(this.mongoose),
            Comments: new CommentModel(this.mongoose),
            Tickets: new TicketsModel(this.mongoose)
        }
    }
}

module.exports = Models;
