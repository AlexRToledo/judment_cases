const BaseCrudRepository = require('../../core/base/BaseCrudRepository');

class OfficeRepository extends BaseCrudRepository {
    constructor(){
        super('Offices');
    }
}

module.exports = OfficeRepository;