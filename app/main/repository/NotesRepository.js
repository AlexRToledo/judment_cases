const BaseCrudRepository = require('../../core/base/BaseCrudRepository');

class NotesRepository extends BaseCrudRepository {
    constructor(){
        super('Notes');
    }
}

module.exports = NotesRepository;