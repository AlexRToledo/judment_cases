const BaseCrudRepository = require('../../core/base/BaseCrudRepository');

class NotificationRepository extends BaseCrudRepository {
    constructor(){
        super('Notifications');
    }
}

module.exports = NotificationRepository;