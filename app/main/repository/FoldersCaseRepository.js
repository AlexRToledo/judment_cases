const BaseCrudRepository = require('../../core/base/BaseCrudRepository');

class FoldersCaseRepository extends BaseCrudRepository {
    constructor(){
        super('FolderCases');
    }
}

module.exports = FoldersCaseRepository;