const BaseCrudRepository = require('../../core/base/BaseCrudRepository');

class CaseRepository extends BaseCrudRepository {
    constructor(){
        super('Cases');
    }
}

module.exports = CaseRepository;