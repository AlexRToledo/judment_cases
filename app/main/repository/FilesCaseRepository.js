const BaseCrudRepository = require('../../core/base/BaseCrudRepository');

class FilesCaseRepository extends BaseCrudRepository {
    constructor(){
        super('FilesCases');
    }
}

module.exports = FilesCaseRepository;