const BaseModel = require('../../core/base/BaseModel');

class NotificationModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'Notification');
    }

    Initialize() {
        let model = new this.Schema({
            notes: {type: String},
            paraphrase: {type: String, required: true},
            case: {type: this.Schema.Types.ObjectId, ref: 'Case', required: true},
            office: {type: this.Schema.Types.ObjectId, ref: 'Office', required: true},
            createdAt: {type: Date, required: true, default: Date},
            closed: {type: Boolean, require: true, default: false},
            removed: {type: Boolean, require: true, default: false}
        });

        return this.mongoose.model(this.name, model);
    }
}

module.exports = NotificationModel;
