const BaseModel = require('../../core/base/BaseModel');

class OfficeModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'Office');
    }

    Initialize() {
        let model = new this.Schema({
            name: {type: String, required: true, unique: true},
            email: { type: String, required: true, unique: true },
            address: { type: String, required: true},
            phone: { type: Number, required: true, unique: true},
            plan: { type: String, required: true, enum: ['SILVER', 'GOLD', 'DIAMOND']},
            extra: {
                users: {type: Number, default: 0},
                admins: {type: Number, default: 0},
                cases: {type: Number, default: 0},
                store: {type: Number, default: 0},
            },
            createdAt: {type: Date, required: true, default: Date},
            removed: {type: Boolean, require: true, default: false},
            status: {type: Boolean, require: true, default: false}
        });

        model.pre('updateOne', async function(next) {
            try {
                const _id = this._conditions._id._id,
                    officeusers = await db.Users.find({ office: _id }).exec(),
                    aux = [];
                if (officeusers.length > 0) {
                    officeusers.forEach(async (ele) => {
                        aux.push(db.Users.update({ _id: ele._id }, { $set: { status: this._update.$set.status } }));
                    });
                    await Promise.all(aux);
                }
                next();
            } catch (err) {console.log(err) }// eslint-disable-line no-empty
        });

        model.pre('deleteOne', async function (next) {
            try {
                const _id = this._conditions._id;
                await db.Users.deleteMany({ office: _id }).exec();
                next();
            } catch (err) {console.log(err) }
        });

        return this.mongoose.model(this.name, model);
    }
}

module.exports = OfficeModel;
