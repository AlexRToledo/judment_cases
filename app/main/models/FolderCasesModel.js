const BaseModel = require('../../core/base/BaseModel');

class FolderCasesModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'FolderCase');
    }

    Initialize() {
        let model = new this.Schema({
            name: {type: String, required: true},
            parent: {type: String, required: true},
            files: [{type: this.Schema.Types.ObjectId, ref: 'FileCase', required: true}],
            case_jud: {type: this.Schema.Types.ObjectId, ref: 'Case', required: true},
            office: {type: this.Schema.Types.ObjectId, ref: 'Office', required: true},
            createdAt: {type: Date, required: true, default: new Date()},
            removed: {type: Boolean, require: true, default: false}
        });

        return this.mongoose.model(this.name, model);
    }
}

module.exports = FolderCasesModel;
