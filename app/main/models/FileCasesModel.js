const BaseModel = require('../../core/base/BaseModel');
class FileCasesModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'FileCase');
    }

    Initialize() {
        let model = new this.Schema({
            name: {type: String, required: true},
            note: {type: String},
            originalname: {type: String, require: true},
            extension: {type: String, required: true},
            size: {type: this.Schema.Types.Mixed, required: true},
            case_jud: {type: this.Schema.Types.ObjectId, ref: 'Case', required: true},
            office: {type: this.Schema.Types.ObjectId, ref: 'Office', required: true},
            parent_folder: {type: this.Schema.Types.ObjectId, ref: 'FolderCase', required: true},
            createdAt: {type: Date, required: true, default: new Date()},
            removed: {type: Boolean, require: true, default: false}
        });

        return this.mongoose.model(this.name, model);
    }
}

module.exports = FileCasesModel;