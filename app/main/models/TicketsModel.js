const BaseModel = require('../../core/base/BaseModel');

class TicketsModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'Ticket');
    }

    Initialize() {
        let model = new this.Schema({
            title: {type: String, required: true, unique: true},
            comments: [{
                type: this.Schema.Types.ObjectId, ref: 'Comment'
            }],
            status: {type: Boolean, require: true, default: false},
            office: {type: this.Schema.Types.ObjectId, ref: 'Office', required: true},
            user: {type: this.Schema.Types.ObjectId, ref: 'User', required: true},
            createdAt: {type: Date, required: true, default: new Date()},
            modifiedAt: {type: Date, required: true, default: new Date()},
            removed: {type: Boolean, require: true, default: false}
        });

        return this.mongoose.model(this.name, model);
    }
}

module.exports = TicketsModel;
