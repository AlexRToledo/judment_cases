const BaseModel = require('../../core/base/BaseModel');

class CommentModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'Comment');
    }

    Initialize() {
        let model = new this.Schema({
            body: {type: String, required: true, unique: true},
            user: {type: this.Schema.Types.ObjectId, ref: 'User', required: true},
            createdAt: {type: Date, required: true, default: new Date()},
            removed: {type: Boolean, require: true, default: false}
        });

        return this.mongoose.model(this.name, model);
    }
}

module.exports = CommentModel;
