const BaseModel = require('../../core/base/BaseModel');

class NotesModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'Note');
    }

    Initialize() {
        let model = new this.Schema({
            text: {type: String, required: true},
            color: {type: String, required: true, enum: ["#f44336", "#4caf50", "#2196f3", "#009688", "#9c27b0", "#ffc107"]},
            date: {type: Date, require: true, default: new Date()},
            timer: {type: Date, require: true, default: new Date()},
            office: {type: this.Schema.Types.ObjectId, ref: 'Office', required: true},
            user: {type: this.Schema.Types.ObjectId, ref: 'User', required: true},
            createdAt: {type: Date, required: true, default: Date},
            removed: {type: Boolean, require: true, default: false}
        });

        return this.mongoose.model(this.name, model);
    }
}

module.exports = NotesModel;
