const bcrypt = require('bcrypt-nodejs'),
      BaseModel = require('../../core/base/BaseModel');

class UserModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'User');
    }

    Initialize() {
        let model = new this.Schema({
            username: {type: String, required: true, unique: true},
            email: { type: String, required: true, unique: true },
            password: {type: String, required: true},
            permissions: {
                isSuper: { type: Boolean, required: true, default: false },
                isAdmin: { type: Boolean, required: true, default: false },
                isUser: { type: Boolean, required: true, default: false }
            },
            status: {type: Boolean, require: true, default: false},
            office: {type: this.Schema.Types.ObjectId, ref: 'Office'},
            createdAt: {type: Date, required: true, default: Date},
            removed: {type: Boolean, require: true, default: false},
        });

        model.methods.validPassword = function (password, next) {
            return new Promise((resolve, reject) => {
                bcrypt.compare(password, this.password, function (err, valid) {
                    resolve({ valid, data: err });
                });
            });
        };

        return this.mongoose.model(this.name, model);
    }
}

module.exports = UserModel;
