const BaseModel = require('../../core/base/BaseModel');

class CaseModel extends BaseModel{
    constructor(mongoose) {
        super(mongoose, 'Case');
    }

    Initialize() {
        let model = new this.Schema({
            name: {type: String, required: true, unique: true},
            court: {type: String, required: true},
            year: { type: String, required: true, default: new Date()},
            case_number: { type: String, required: true},
            notes: {type: String},
            involved: {type: String, default: ''},
            users_follow: [{
                type: this.Schema.Types.ObjectId, ref: 'User'
            }],
            status: {type: Boolean, require: true, default: false},
            office: {type: this.Schema.Types.ObjectId, ref: 'Office', required: true},
            createdAt: {type: Date, required: true, default: new Date()},
            removed: {type: Boolean, require: true, default: false}
        });

        return this.mongoose.model(this.name, model);
    }
}

module.exports = CaseModel;
