const CronJob = require('cron').CronJob,
      Fs = require('fs'),
      PDFParser = require("pdf2json"),
      Path = require('path'),
      Axios = require('axios'),
      DateParse = require('../../utils/DatePDF'),
      FindInFiles = require('find-in-files'),
      CaseRepository = require('../../repository/CaseRepository'),
      Notification = require('../../repository/NotificationRepository');

class CronSchedule {
    constructor() {
        this.Initialize();
    }

    async Initialize() {
        // cron ss mm hh - - (day of week)
        new CronJob(
            '00 00 01 * * *',
            async () => {
                //await this.DownloadDoc();
                //await this.PDFParse();
                await this.CasesJob();
            },
            null,
            true,
            'America/Mexico_City'
        );
    }

    async PDFParse() {
        try {
            const DateP = new DateParse(),
                date = DateP.DateWithDash(),
                path = Path.resolve(__dirname, './files');
            let pdfParser = new PDFParser(this,1);
            pdfParser.on("pdfParser_dataError", errData => {
                console.error(errData.parserError)
            });
            pdfParser.on("pdfParser_dataReady", pdfData => {
                Fs.writeFileSync(`${path}/${date}.txt`, pdfParser.getRawTextContent());
            });

            pdfParser.loadPDF(`${path}/${date}.pdf`);
        } catch (err) {
            console.log(err)
        }
    }

    async DownloadDoc() {
        const DateP = new DateParse(),
            date = DateP.DateWithDash(),
            url = `http://www.yucatan.gob.mx/docs/diario_oficial/diarios/${new Date().getFullYear()}/${date}_1.pdf`,
            path = Path.resolve(__dirname, './files', `${date}.pdf`),
            writer = Fs.createWriteStream(path);

        const response = await Axios({
            url,
            method: 'GET',
            responseType: 'stream'
        });

        response.data.pipe(writer);

        return new Promise((resolve, reject) => {
            writer.on('finish', resolve);
            writer.on('error', reject);
        })
    }

    async CasesJob() {
        const Repository = new CaseRepository(),
            cases = await Repository.FindAll({removed: false, status: false});
        cases.forEach(async (exp) => {
            await this.SearchInFile(exp);
        });
    }

    async SearchInFile(exp) {
        const DateP = new DateParse(),
            date = DateP.DateWithDash(),
            findjudment = await FindInFiles.find(exp.court.toUpperCase(), `.`, `${date}.txt$`),
            Repository = new Notification();
        if(Object.entries(findjudment).length !== 0) {
            let findCaseNumber = await FindInFiles.find(exp.case_number, '.', `${date}.txt$`);
            if(Object.entries(findCaseNumber).length !== 0) {
                let findPerson = await FindInFiles.find(exp.involved.toUpperCase(), '.', `${date}.txt$`);
                if(Object.entries(findPerson).length !== 0) {
                    console.log("Exist!");
                    await Repository.Create({
                        case: exp._id,
                        office: exp.office,
                        paraphrase: 'Exist coincidence.'
                    });
                    return true;
                } else {
                    console.log("Case file doesn't exits!");
                    return false;
                }
            } else {
                console.log("Person doesn't exits!");
                return false;
            }
        } else {
            console.log("Judment doesn't exist!");
            return false;
        }
    }
}

module.exports = CronSchedule;