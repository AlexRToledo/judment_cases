const plans = require('../../utils/Plans');

class PlanWrapper {
    constructor(plan) {
        this.plan = plans[plan];
    }

    checkUsersValidity(count, plus_users) {
        try {
            if (count >= this.plan.max_users + plus_users) {
                return false;
            }
            return true;
        } catch (err) {
            return null
        }
    }

    checkUsersAdminValidity(count, plus_admins) {
        try {
            if (count >= this.plan.max_admins + plus_admins) {
                return false;
            }
            return true;
        } catch (err) {
            return null
        }
    }

    checkCasesValidity(count, plus_cases) {
        try {
            if(this.plan.max_cases === -1) {
                return true;
            } else {
                if (count >= this.plan.max_cases + plus_cases) {
                    return false;
                }
                return true;
            }
        } catch (err) {
            return null
        }
    }

    checkStorageValidity(total, plus_store, new_value) {
        try {
            if (total.length > 0) {
                if(Math.round((total[0].amount + new_value) * 1000) / 1000 >= parseInt(this.plan.max_space) + plus_store) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } catch (err) {
            return null
        }
    }

}

module.exports = PlanWrapper;