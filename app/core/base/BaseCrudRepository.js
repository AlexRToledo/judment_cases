'use strict';

class BaseCrudRepository {

    constructor(model) {
        this._model = model;
    }

    async Aggregate(condition={}) {
        let myModel = db[this._model];

        const query = myModel.aggregate(condition);

        if (config.start)
            query.skip(+config.start);

        if (config.length)
            query.limit(+config.length);

        if (config.sort)
            query.sort(config.sort);

        return await query.exec();
    }


    async FindAll(condition={}, limit = null, skip = null, populate = null, sort = null) {

        condition.removed = false;

        let myModel = db[this._model];

        let query = myModel.find(condition);
        if ( limit )
            query.limit(+limit);
        if ( skip )
            query.skip(+skip);
        if ( populate )
            query.populate(populate);
        if( sort )
            query.sort(sort);

        return await query.exec();
    }

    async Count(condition = {}) {
        condition.removed = false;

        let myModel = db[this._model];

        return await myModel.countDocuments(condition).exec()
    }

    async Find(condition = {}, populate = null) {
        condition.removed = false;

        let myModel = db[this._model];

        let query = myModel.findOne(condition);

        if (populate) {
            query.populate(populate);
        }

        return await query.exec();
    }

    async Create(record) {
        let myModel = new db[this._model](record);

        return await myModel.save();
    }

    async Remove(id) {
        let myModel = db[this._model];

        return await myModel.updateOne({_id: id}, {$set: {removed: true}});
    }

    async Erase(id) {
        let myModel = db[this._model];

        return await myModel.deleteOne({_id: id});
    }

    async Update(id, record) {
        let myModel = db[this._model];

        return await myModel.updateOne({removed: false, _id: id}, {$set: record});
    }

    async EmptyRecord() {
        let myModel = db[this._model];
        return new myModel();
    }
}

module.exports = BaseCrudRepository;
